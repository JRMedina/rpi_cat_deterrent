import time
import pygame
from pygame import mixer

from picamera import PiCamera


if __name__ == "__main__":
    # Instantiate mixer, sound, and camera
    pygame.mixer.init()
    sound = mixer.Sound("sax_attack.wav")
    camera = PiCamera()

    camera.start_preview()

    start = time.time()

    for i in range(2):
        time.sleep(5)
        sound.play()
        camera.capture("capture_image_%s.jpg" % i)

    camera.stop_preview()

