import time
import pygame
from pygame import mixer


if __name__ == "__main__":
    # Instantiate mixer and sound
    pygame.mixer.init()
    sound = mixer.Sound("loud_sound.wav")

    start = time.time()

    # Continue playing sound forever
    while True:
        end = time.time()

        # Play every second
        if end - start >= 1.0:
            sound.play()
            start = time.time()

