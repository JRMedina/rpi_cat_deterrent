from ubuntu:latest

ENV LANG C.UTF-8
ARG APT_INSTALL="apt-get install -y"
ARG PIP_INSTALL="python -m pip --no-cache-dir install --upgrade"
ARG GIT_CLONE="git clone --depth 10"

RUN rm -rf /var/lib/apt/lists/* \
    /etc/apt/sources.list.d/cuda.list \
    /etc/apt/sources.list.d/nvidia-ml.list;

RUN apt-get update && yes | apt-get upgrade;

# ==================================================================
# Tools
# ------------------------------------------------------------------

RUN DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
    build-essential \
    apt-utils \
    ca-certificates \
    wget \
    git \
    vim;

# ==================================================================
# Python
# ------------------------------------------------------------------
RUN DEBIAN_FRONTEND=noninteractive $APT_INSTALL software-properties-common; \
    add-apt-repository ppa:deadsnakes/ppa; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
    python3.6 \
    python3.6-dev \
    python3-distutils-extra; \
    wget -O ~/get-pip.py https://bootstrap.pypa.io/get-pip.py; \
    python3.6 ~/get-pip.py; \
    ln -s /usr/bin/python3.6 /usr/local/bin/python3; \
    ln -s /usr/bin/python3.6 /usr/local/bin/python; \
    $PIP_INSTALL setuptools \
    pygame;


# ==================================================================
# Copy Program Files
# ------------------------------------------------------------------
COPY rpi_cat_deterrent /rpi_cat_deterrent